Source: r-bioc-geneplotter
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Michael R. Crusoe <crusoe@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-geneplotter
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-geneplotter.git
Homepage: https://bioconductor.org/packages/geneplotter/
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-biobase,
               r-bioc-biocgenerics,
               r-cran-lattice,
               r-bioc-annotate,
               r-bioc-annotationdbi,
               r-cran-rcolorbrewer
Testsuite: autopkgtest-pkg-r

Package: r-bioc-geneplotter
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: R package of functions for plotting genomic data
 Geneplotter contains plotting functions for microarrays.
 .
 The functions cPlot and cColor allow the user to
 associate microarray expression data with chromosomal location.
 The plots can include any subset (by default all chromosomes are
 shown) of chromosomes for the organism being considered.
